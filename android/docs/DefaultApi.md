# DefaultApi

All URIs are relative to *https://virtserver.swaggerhub.com/fbmattos2/temp2/1.0.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**helloGreetingGET**](DefaultApi.md#helloGreetingGET) | **GET** /hello/greeting | 
[**helloUserTimeOfDayGET**](DefaultApi.md#helloUserTimeOfDayGET) | **GET** /hello/{user}/{timeOfDay} | 


<a name="helloGreetingGET"></a>
# **helloGreetingGET**
> List&lt;String&gt; helloGreetingGET()



Returns a list of greetings.

### Example
```java
// Import classes:
//import io.swagger.client.api.DefaultApi;

DefaultApi apiInstance = new DefaultApi();
try {
    List<String> result = apiInstance.helloGreetingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#helloGreetingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**List&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="helloUserTimeOfDayGET"></a>
# **helloUserTimeOfDayGET**
> String helloUserTimeOfDayGET(user, timeOfDay)



Returns a greeting to the user!

### Example
```java
// Import classes:
//import io.swagger.client.api.DefaultApi;

DefaultApi apiInstance = new DefaultApi();
String user = "user_example"; // String | The name of the user to greet.
String timeOfDay = "timeOfDay_example"; // String | The time of the day (morning, afternoon, evening, night)
try {
    String result = apiInstance.helloUserTimeOfDayGET(user, timeOfDay);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#helloUserTimeOfDayGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | **String**| The name of the user to greet. |
 **timeOfDay** | **String**| The time of the day (morning, afternoon, evening, night) |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

